using System;
using System.Text;
using System.Net;
using System.Collections;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Collections.Specialized;
using System.Web;
using System.Diagnostics;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace HttpListenerExample
{
    using SessionID = String;

    class User
    {
        public string email;
        public string password;
        public User(string email, string password)
        {
            this.email = email;
            this.password = password;
        }

        public override string ToString()
        {
            return "User { " + email + ", " + password + " };";
        }
    }

    class ResponseMessageRedirect : ResponseMessageBase 
    {
        public string Url { get; set; }
        public ResponseMessageRedirect(string url)
        {
            TypeMessage = Type.redirect;
            Url = url;
        }
    }

    class ResponseMessageError : ResponseMessageBase
    {
        public string[] Errors { get; set; }
        public ResponseMessageError(params string[] errors)
        {
            TypeMessage = Type.error;
            Errors = errors;
        }
    }

    class ResponseMessageBase
    {
        public enum Type { redirect, error }
        [JsonPropertyName("type")]
        public Type TypeMessage { get; set; }

        public string ToJson()
        {
            var serializeOptions = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                WriteIndented = true,
                Converters =
                {
                    new JsonStringEnumConverter(JsonNamingPolicy.CamelCase)
                }
            };

            return JsonSerializer.Serialize(this, this.GetType(), serializeOptions);
        }
    }

    abstract class RouteHandler
    {
        public abstract string Get(HttpListenerRequest requset, HttpListenerResponse response);
        public abstract string Post(HttpListenerRequest requset, HttpListenerResponse response);
        public virtual bool IsAuthorized(HttpListenerRequest request)
        {
            return HttpServer.IsAuthorized(request);
        }

        public virtual bool NeedAuthorization(HttpListenerRequest request)
        {
            return true;
        }
    }

    class Root : RouteHandler
    {

        public override bool NeedAuthorization(HttpListenerRequest request)
        {
            return false;
        }

        public override string Get(HttpListenerRequest requset, HttpListenerResponse response)
        {
            return HttpServer.GetPage("index.html");
        }

        public override string Post(HttpListenerRequest requset, HttpListenerResponse response)
        {
            throw new NotImplementedException();
        }
    }

    class Account : RouteHandler
    {
        public override string Get(HttpListenerRequest requset, HttpListenerResponse response)
        {
            User user = HttpServer.GetCurrentUser(requset);
            return HttpServer.GetAccountPage(user);
        }

        public override string Post(HttpListenerRequest requset, HttpListenerResponse response)
        {
            throw new NotImplementedException();
        }
    }

    class SignIn : RouteHandler
    {
        public override bool NeedAuthorization(HttpListenerRequest request)
        {
            return false;
        }

        public override string Get(HttpListenerRequest request, HttpListenerResponse response)
        {
            if (HttpServer.IsAuthorized(request))
            {
                response.Redirect("/account");
            }
            else
            {
                return HttpServer.GetPage("sign_in.html");
            }

            return "";
        }

        public override string Post(HttpListenerRequest request, HttpListenerResponse response)
        {
            NameValueCollection formData = HttpServer.ParseBody(request);
            string email = formData.Get("email")!;
            string password = formData.Get("password")!;

            if (String.IsNullOrEmpty(email)) {
                return new ResponseMessageError("Email cannot be empty").ToJson();
            } else if (String.IsNullOrEmpty(password)) {
                return new ResponseMessageError("Password cannot be empty").ToJson();
            } 

            if (HttpServer.emailToUser.Contains(email))
            {
                int userIndex = (int)HttpServer.emailToUser[email]!;
                User user = HttpServer.users[userIndex];

                if (user.password == password)
                {
                    SessionID sessionID = Guid.NewGuid().ToString();
                    Cookie cookie = new Cookie(HttpServer.COOKIE_KEY, sessionID);
                    HttpServer.sessionIDToUser.Add(sessionID, userIndex);
                    response.Cookies.Add(cookie);

                    return new ResponseMessageRedirect("/account").ToJson();
                }
            }

            return new ResponseMessageError("Email or password incorrect").ToJson();
        }
    }

    class SignUp : RouteHandler
    {
        public override bool NeedAuthorization(HttpListenerRequest request)
        {
            return false;
        }

        public override string Get(HttpListenerRequest request, HttpListenerResponse response)
        {
            if (HttpServer.IsAuthorized(request))
            {
                return HttpServer.GetAccountPage(null);
            }
            else
            {
                return HttpServer.GetPage("sign_up.html");
            }
        }

        public override string Post(HttpListenerRequest request, HttpListenerResponse response)
        {
            NameValueCollection formData = HttpServer.ParseBody(request);
            string email = formData.Get("email")!;
            string password = formData.Get("password")!;

            if (String.IsNullOrEmpty(email)) {
                return new ResponseMessageError("Email cannot be empty").ToJson();
            } else if (String.IsNullOrEmpty(password)) {
                return new ResponseMessageError("Password cannot be empty").ToJson();
            }

            if (HttpServer.emailToUser.Contains(email)) {
                return new ResponseMessageError("User alredy exist").ToJson();
            } else {
                User newUser = new User(email, password);
                HttpServer.users.Add(newUser);
                int newUserIndex = HttpServer.users.Count - 1;

                SessionID sessionId = Guid.NewGuid().ToString();
                HttpServer.sessionIDToUser.Add(sessionId, newUserIndex);
                HttpServer.emailToUser.Add(email, newUserIndex);

                Cookie cookie = new Cookie(HttpServer.COOKIE_KEY, sessionId);
                response.Cookies.Add(cookie);
            }

            return new ResponseMessageRedirect("/account").ToJson();
        }
    }

    class LogOut : RouteHandler
    {
        public override string Get(HttpListenerRequest request, HttpListenerResponse response)
        {
            Cookie cookie = new Cookie(HttpServer.COOKIE_KEY, null)
            {
                Expires = DateTime.Now.AddDays(-1)
            };
            response.SetCookie(cookie);

            response.Redirect("/");

            return "";
        }

        public override bool NeedAuthorization(HttpListenerRequest request)
        {
            return false;
        }

        public override string Post(HttpListenerRequest requset, HttpListenerResponse response)
        {
            throw new NotImplementedException();
        }
    }

    class Invalid : RouteHandler
    {
        public override string Get(HttpListenerRequest request, HttpListenerResponse response)
        {
            return HttpServer.GetInvalidPage("Incorrect email or password");
        }

        public override bool NeedAuthorization(HttpListenerRequest request)
        {
            return false;
        }

        public override string Post(HttpListenerRequest requset, HttpListenerResponse response)
        {
            throw new NotImplementedException();
        }
    }

    class HttpServer
    {
        public static HttpListener listener;
        public const string URL = "http://localhost:8000/";
        public static string COOKIE_KEY = "localhost";
        public static List<User> users = new List<User>();
        public static IDictionary sessionIDToUser = new Dictionary<SessionID, int>();
        public static IDictionary emailToUser = new Dictionary<string, int>();
        public static IDictionary RouteGetHandlers = new Dictionary<string, RouteHandler>();
        public static IDictionary RoutePostHandlers = new Dictionary<string, RouteHandler>();

        public static async Task HandleIncomingConnections()
        {
            string page = GetPage("index.html");

            RouteGetHandlers.Add("/", new Root());
            RouteGetHandlers.Add("/account", new Account());
            RouteGetHandlers.Add("/sign_in", new SignIn());
            RouteGetHandlers.Add("/sign_up", new SignUp());
            RouteGetHandlers.Add("/logout", new LogOut());
            RouteGetHandlers.Add("/invalid", new Invalid());

            RoutePostHandlers.Add("/sign_in", new SignIn());
            RoutePostHandlers.Add("/sign_up", new SignUp());

            while (true)
            {
                HttpListenerContext httpListenerContext = await listener.GetContextAsync();

                HttpListenerRequest request = httpListenerContext.Request;
                HttpListenerResponse response = httpListenerContext.Response;

                if (request.HttpMethod == "GET" && request.Url.AbsolutePath.StartsWith("/assets/"))
                {
                    ServerAssets(request.Url.AbsolutePath, response);
                    continue;
                }

                Console.WriteLine(request.Url.ToString());
                Console.WriteLine(request.HttpMethod);
                Console.WriteLine(request.UserHostName);
                Console.WriteLine();

                page = HandleIncomingRequest(request, response);

                response.ContentType = "text/html";
                response.ContentEncoding = Encoding.UTF8;

                if (response.Headers["Location"] == null)
                {
                    byte[] data = Encoding.UTF8.GetBytes(page);
                    response.ContentLength64 = data.LongLength;
                    await response.OutputStream.WriteAsync(data);
                }

                response.Close();
            }
        }

        public static string HandleIncomingRequest(HttpListenerRequest request, HttpListenerResponse response)
        {
            IDictionary RouteMap;
            switch (request.HttpMethod)
            {
                case "GET":
                    RouteMap = RouteGetHandlers;
                    break;
                case "POST":
                    RouteMap = RoutePostHandlers;
                    break;
                default:
                    throw new NotImplementedException();
            }

            RouteHandler RouteHandler = (RouteHandler)RouteMap[request.Url.AbsolutePath];
            if (RouteHandler == null)
            {
                return GetInvalidPage("404 NOT FOUND");
            }

            if (RouteHandler.NeedAuthorization(request) && !RouteHandler.IsAuthorized(request))
            {
                response.Redirect("/sign_in");
                return "";
            }

            switch (request.HttpMethod)
            {
                case "GET":
                    return RouteHandler.Get(request, response);
                case "POST":
                    return RouteHandler.Post(request, response);
                default:
                    throw new NotImplementedException();
            }
        }

        public static bool IsAuthorized(HttpListenerRequest request)
        {
            if (request.Cookies[COOKIE_KEY] == null)
            {
                return false;
            }

            SessionID sessionID = request.Cookies[COOKIE_KEY].Value;
            return sessionIDToUser.Contains(sessionID);
        }

        public static User GetCurrentUser(HttpListenerRequest request)
        {
            SessionID sessionID = request.Cookies[COOKIE_KEY]?.Value;
            int userIndex = (int)sessionIDToUser[sessionID];
            User user = users[userIndex];

            Debug.Assert(user != null, String.Format("Wtf where is user {0}: {1}", sessionID, userIndex));
            return user;
        }

        public static NameValueCollection ParseBody(HttpListenerRequest req)
        {
            Stream body = req.InputStream;
            Encoding encoding = req.ContentEncoding;
            StreamReader reader = new StreamReader(body, encoding);
            NameValueCollection vars = HttpUtility.ParseQueryString(reader.ReadToEnd());

            body.Close();
            reader.Close();

            return vars;
        }

        public static string GetAccountPage(User user)
        {
            return String.Format(GetPage("/account.html"), user.email);
        }

        public static string GetInvalidPage(string messageError)
        {
            return String.Format(GetPage("/invalid.html"), messageError);
        }

        public static string GetPage(string fileName)
        {
            return File.ReadAllText(
                String.Format(
                    "{0}{1}{2}",
                    Environment.CurrentDirectory,
                    "/templates/", fileName
                )
            );
        }

        public static string GetAsset(string fileName)
        {
            return File.ReadAllText(
                String.Format("{0}{1}", Environment.CurrentDirectory, fileName)
            );
        }

        public static void ServerAssets(string fileName, HttpListenerResponse response)
        {
            response.ContentType = GetMimeType(fileName);
            response.ContentEncoding = Encoding.UTF8;

            byte[] data = Encoding.UTF8.GetBytes(GetAsset(fileName));
            response.ContentLength64 = data.LongLength;
            response.OutputStream.Write(data, 0, data.Length);

            response.Close();
        }

        public static string GetMimeType(string fileName)
        {
            string type = fileName.Split('.')[1];
            return type switch
            {
                "css" => "text/css",
                _ => "text/javascript",
            };
        }

        public static void printUsers(List<User> users)
        {
            foreach (User user in users)
            {
                Console.WriteLine(user);
            }
        }

        public static void Main()
        {
            listener = new HttpListener();
            listener.Prefixes.Add(URL);
            listener.Start();
            Console.WriteLine("Listening for connections on {0}", URL);

            Task listenTask = HandleIncomingConnections();
            listenTask.GetAwaiter().GetResult();

            listener.Close();
        }
    }
}
