﻿window.addEventListener("DOMContentLoaded", () => {
    const input_email = document.getElementById("email");
    const input_password = document.getElementById("password");
    const error_message = document.getElementById("error");
    const signin_button = document.getElementById("send");
    const form = document.getElementById("sign_in");

    signin_button.addEventListener("click", (event) => {
        event.preventDefault();
        const data = new FormData(form);
        fetch("/sign_in", { method: "POST", body: new URLSearchParams(data) })
            .then(data => data.json())
            .then(response => {
                if (response.type === "redirect") {
                    location.href = '/account';
                } else {
                    error_message.innerHTML = response.errors;
                    error_message.classList.remove("hide"); 
                }
            }).catch(() => {
                error_message.innerHTML = "Something wrong!"
                error_message.classList.remove("hide"); 
            })
    })
        
    function clearError() {
        error_message.classList.add("hide");
    }

    input_email.addEventListener("input", clearError)
    input_password.addEventListener("input", clearError);
});
