﻿window.addEventListener("DOMContentLoaded", () => {
    const input_email = document.getElementById("email");
    const input_password = document.getElementById("password");
    const input_password_confirmation = document.getElementById("password_confirmation");
    const error_email_message = document.getElementById("error_email");
    const error_password_message = document.getElementById("error_password");
    const signup_button = document.getElementById("send");
    const form = document.getElementById("sign_up");

    signup_button.addEventListener("click", (event) => {
        const password = input_password.value;
        const password_confirmation = input_password_confirmation.value;

        if (password !== password_confirmation) {
            event.preventDefault();
            input_password.focus();
            error_password_message.classList.remove("hide");
            return;
        }

        event.preventDefault();
        const data = new FormData(form);
        fetch("/sign_up", {method: "POST", body: new URLSearchParams(data)})
            .then(data => data.json())
            .then(response => {
                if (response.type === "redirect") {
                    location.href = '/account';
                } else {
                    input_email.focus();
                    error_email_message.innerHTML = response.errors;
                    error_email_message.classList.remove("hide");  
                }
            }).catch(() => {
                error_email_message.innerHTML = "Something wrong!"
                error_email_message.classList.remove("hide"); 
            })
    });

    function clearErrorEmail() {
        error_email_message.classList.add("hide");
    }

    function clearErrorPass() {
        error_password_message.classList.add("hide");
    }

    input_email.addEventListener("input", clearErrorEmail)
    input_password.addEventListener("input", clearErrorPass);
    input_password_confirmation.addEventListener("input", clearErrorPass);
});
